#ifndef FCAM_F2_PLATFORM
#define FCAM_F2_PLATFORM

#include "FCam/Platform.h"

/** \file
 * Static platform data about the F2 and its main image sensor.
 */

namespace FCam {
namespace F2 {

/** Static platform data about the F2 and its main image sensor. */
class Platform : public FCam::Platform {
public:
    virtual void rawToRGBColorMatrix(int kelvin, float *matrix) const;
    virtual const std::string &manufacturer() const {
        static std::string s("Stanford University"); return s;
    }
    virtual const std::string &model() const {
        static std::string s("F2 Frankencamera"); return s;
    }

    /** The F2 produces 10-bit raw values, always positive */
    virtual unsigned short minRawValue() const { return 0; }
    /** The F2 produces 10-bit raw values, always positive */
    virtual unsigned short maxRawValue() const { return 1024; }

    /** The F2's bayer pattern (the top-left 2x2 block of pixels is GRBG */
    virtual BayerPattern bayerPattern() const { return GRBG; }

    /** Access to the singleton instance of this class. Normally
     * you access the platform data via Frame::platform or
     * Sensor::platform. */
    static const Platform &instance();
};

}
}

#endif
