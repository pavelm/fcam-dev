#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <time.h>
#include <poll.h>
#include <sstream>

#include "FCam/Time.h"
#include "FCam/Frame.h"
#include "FCam/Action.h"
#include "FCam/FCam.h"

#include "../Debug.h"
#include "Daemon.h"
#include "linux/omap34xxcam-fcam.h"

/* sdl */
#include <iostream>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include <math.h>

int d_raw;
  int d_argc;
  char **d_argv;

namespace FCam {
namespace N900 {

  typedef struct {
    Uint8 r;
    Uint8 g;
    Uint8 b;
    Uint8 alpha;
  } pixel;

  static inline int brightness(unsigned char *p)
  {
    if (!d_raw)
      return *(p+1);
      
    if (*(p+1) & ~3UL)
      printf("????\n");

    return *(p) + ((*(p+1) & 3UL) << 8); 
  }

  class Conv {
  public:
    void print(int gain, int exp) {
      double iso = 100 * pow(2, gain/10.);
      /* 50 ... 0.5 msec */
      double time = exp / 100000.;

      time = 1./time;

      printf("ISO %.f 1/%.f sec %.1f EV", iso, time, -2+ISO_to_EV(iso) + time_to_EV(time));
    } 

    double ISO_to_EV(double v) {
      return -(log(v / 100.)/log(2));
    }

    /* Takes inverse 1/time! */
    double time_to_EV(double v) {
      return 5+log(v)/log(2);
    }

    double focus_to_m(double p) {
      if (p == 0)
	return 999999;
      return 1023./(p*20);
    }

    double focus_to_diopter(double p) {
      return (p/1023.) * 20;
    }

    double diopter_to_m(double p) {
      if (p == 0)
	return 999999;
      return 1./p;
    }
  };

  class SDLOut {
  public:

    SDL_Window* window;
    SDL_Surface *screen;
    int sx, sy;
    int factor;
    float focus, gain, time; 

    SDLOut(FCam::Image &img) {
      printf("Initing SDL\n");

      if (SDL_Init(SDL_INIT_VIDEO) < 0) { //Could we start SDL_VIDEO?              
	std::cerr << "Couldn't init SDL"; //Nope, output to stderr and quit
  exit(1);
      }

      atexit(SDL_Quit); //Now that we're enabled, make sure we cleanup           

      factor = 8;
      sx = img.size().width / factor;
      sy = img.size().height / factor;
      window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, 
				 SDL_WINDOWPOS_UNDEFINED, sx, sy, SDL_WINDOW_SHOWN );
      if (window == NULL) {
	printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
      }

      screen = SDL_GetWindowSurface( window );
      if (!screen) { //Couldn't create window?                                     
	std::cerr << "Couldn't create screen"; //Output to stderr and quit
	exit(1);
      }
    }

    void loop(void) {
      int done;
      SDL_Event event;

      while(!done){ //While program isn't done                                   
	while(SDL_PollEvent(&event)){ //Poll events                        
	  switch(event.type){ //Check event type                     
	  case SDL_QUIT: //User hit the X (or equivelent)            
	    done = true; //Make the loop end                   
	    break; //We handled the event                      
	  } //Finished with current event                            
	} //Done with all events for now                                   
      } //Program done, exited                                                   
    }

    void put_pixel(SDL_Surface* screen, int x, int y, pixel *p)
    {
      Uint32* p_screen = (Uint32*)screen->pixels;
      p_screen += y*screen->w+x;
      *p_screen = SDL_MapRGBA(screen->format,p->r,p->g,p->b,p->alpha);
    }

    int render_statistics(SDL_Surface* screen)
    {
      pixel white;
      white.r = (Uint8)0xff;
      white.g = (Uint8)0xff;
      white.b = (Uint8)0xff;
      white.alpha = (Uint8)128;

      for (int x=0; x<sx && x<sx*focus; x++)
	put_pixel(screen, x, 10, &white);

      for (int x=0; x<sx && x<sx*gain; x++)
	put_pixel(screen, x, 20, &white);

      for (int x=0; x<sx && x<sx*time; x++)
	put_pixel(screen, x, 30, &white);

      for (int x=0; x<sx; x++)
	put_pixel(screen, x, 40, &white);

      return 0;
    }

    void render(FCam::Image &img) {
      //Fill the surface white                                                   
      SDL_FillRect( screen, NULL, SDL_MapRGB( screen->format, 0, 0, 0 ) );

      SDL_LockSurface(screen);
      /*do your rendering here*/

      for (int y = 0; y < sy; y++)
	for (int x = 0; x < sx; x++) {
	  pixel p = { 0, 0, 0, 0 };
	  int b = brightness(img(factor*x, factor*y));
	  if (d_raw) {
	  b /= 4;
	  if ((y % 2) == 0) {
	  switch (x % 2) {
	  case 0: p.g = b; break;
	  case 1: p.r = b; break;
	  }
	  } else {
	  switch (x % 2) {
	  case 0: p.b = b; break;
	  case 1: p.g = b; break;
	  }
	  }
	  } else {
	  p.r = b;
	  p.g = b;
	  p.b = b;
	  }

	  p.alpha = 128;
	  put_pixel(screen, x, y, &p);
	}


      render_statistics(screen);

      /*----------------------*/
      SDL_UnlockSurface(screen);

      //Update the surface                                                       
      SDL_UpdateWindowSurface( window );
    }
  };

void *daemon_setter_thread_(void *arg) {
    Daemon *d = (Daemon *)arg;
    d->runSetter();
    d->setterRunning = false;
    close(d->daemon_fd);
    pthread_exit(NULL);
}

void *daemon_handler_thread_(void *arg) {
    Daemon *d = (Daemon *)arg;
    d->runHandler();
    d->handlerRunning = false;
    pthread_exit(NULL);
}

void *daemon_action_thread_(void *arg) {
    Daemon *d = (Daemon *)arg;
    d->runAction();
    d->actionRunning = false;
    pthread_exit(NULL);
}

  /* video6 for preview mode, video2 for raw capture */
#define DEV_VIDEO "/dev/video_cam"

Daemon::Daemon(Sensor *sensor) :
    sensor(sensor),
    stop(false),
    frameLimit(128),
    dropPolicy(Sensor::DropNewest),
    setterRunning(false),
    handlerRunning(false),
    actionRunning(false),
    threadsLaunched(false) {

    // tie ourselves to the correct sensor
    v4l2Sensor = V4L2Sensor::instance(DEV_VIDEO);

    // make the mutexes for the producer-consumer queues
    if ((errno =
             -(pthread_mutex_init(&actionQueueMutex, NULL) ||
               pthread_mutex_init(&cameraMutex, NULL)))) {
        error(Event::InternalError, sensor, "Error creating mutexes: %d", errno);
    }

    // make the semaphore
    sem_init(&actionQueueSemaphore, 0, 0);

    lastGoodShot.wanted = false;

    for (int i = 0; i < 1024; i++)
      sharpnessMap[i] = -1;
    currentFocus = 0;
    currentGain = 0;
    currentExposure = 50;
    for (int i = 1; i < d_argc; i++) {
      if (((std::string) d_argv[i] ) == "dark") {
	currentExposure = 34000;
	currentGain = 40;
      }
    }
    subphase = 0;
    phase = 0;

    pipelineFlush = true;
}

void Daemon::launchThreads() {
    if (threadsLaunched) { return; }
    threadsLaunched = true;

    // launch the threads
    pthread_attr_t attr;
    struct sched_param param;

    // Open the device as a daemon
    daemon_fd = open(DEV_VIDEO, O_RDWR, 0);

    if (daemon_fd < 0) {
      error(Event::InternalError, sensor, "Error opening %s: %d", DEV_VIDEO, errno);
        return;
    }

    focus_fd = -1;
    gain_fd = -1;

    // Try to register myself as the fcam camera client
    if (ioctl(daemon_fd, VIDIOC_FCAM_INSTALL, NULL)) {
        if (errno == EBUSY) {
            error(Event::DriverLockedError, sensor,
                  "An FCam program is already running");
        } else {
#if 0
            error(Event::DriverMissingError, sensor,
                  "Error %d calling VIDIOC_FCAM_INSTALL: Are the FCam drivers installed?", errno);
#endif
        }
    }
    // I should now have CAP_SYS_NICE

    // make the setter thread
    param.sched_priority = sched_get_priority_min(SCHED_FIFO)+1;

    pthread_attr_init(&attr);

    if ((errno =
             -(/* pthread_attr_setschedparam(&attr, &param) ||
               pthread_attr_setschedpolicy(&attr, SCHED_FIFO) ||*/
               pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) || 
               pthread_create(&setterThread, &attr, daemon_setter_thread_, this)))) {
        error(Event::InternalError, sensor, "Error creating daemon setter thread: %d", errno);
        return;
    } else {
        setterRunning = true;
    }

    // make the handler thread
    param.sched_priority = sched_get_priority_min(SCHED_FIFO);

    if ((errno =
             -(/* pthread_attr_setschedparam(&attr, &param) ||
		  pthread_attr_setschedpolicy(&attr, SCHED_FIFO) || */
               pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) ||
               pthread_create(&handlerThread, &attr, daemon_handler_thread_, this)))) {
        error(Event::InternalError, sensor, "Error creating daemon handler thread: %d", errno);
        return;
    } else {
        handlerRunning = true;
    }

    // make the actions thread
    param.sched_priority = sched_get_priority_max(SCHED_FIFO);

    if ((errno =
             -(/* pthread_attr_setschedparam(&attr, &param) ||
		  pthread_attr_setschedpolicy(&attr, SCHED_FIFO) || */
               pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) ||
               pthread_create(&actionThread, &attr, daemon_action_thread_, this)))) {
        error(Event::InternalError, sensor, "Error creating daemon action thread: %d", errno);
        return;
    } else {
        actionRunning = true;
    }

    pthread_attr_destroy(&attr);
}

Daemon::~Daemon() {
    stop = true;

    // post a wakeup call to the action thread
    sem_post(&actionQueueSemaphore);

    if (setterRunning) {
        pthread_join(setterThread, NULL);
    }

    if (handlerRunning) {
        pthread_join(handlerThread, NULL);
    }

    if (actionRunning) {
        pthread_join(actionThread, NULL);
    }

    pthread_mutex_destroy(&actionQueueMutex);
    pthread_mutex_destroy(&cameraMutex);

    // Clean up all the internal queues
    while (inFlightQueue.size()) { delete inFlightQueue.pull(); }
    while (requestQueue.size()) { delete requestQueue.pull(); }
    while (frameQueue.size()) { delete frameQueue.pull(); }
    while (actionQueue.size()) {
        delete actionQueue.top().action;
        actionQueue.pop();
    }

    v4l2Sensor->stopStreaming();

    v4l2Sensor->close();
}


void Daemon::setDropPolicy(Sensor::DropPolicy p, int f) {
    dropPolicy = p;
    frameLimit = f;
    enforceDropPolicy();
}

void Daemon::enforceDropPolicy() {
    if (frameQueue.size() > frameLimit) {
        warning(Event::FrameLimitHit, sensor,
                "WARNING: frame limit hit (%d), silently dropping %d frames.\n"
                "You're not draining the frame queue quickly enough. Use longer \n"
                "frame times or drain the frame queue until empty every time you \n"
                "call getFrame()\n", frameLimit, frameQueue.size() - frameLimit);
        if (dropPolicy == Sensor::DropOldest) {
            while (frameQueue.size() >= frameLimit) {
                sensor->decShotsPending();
                delete frameQueue.pull();
            }
        } else if (dropPolicy == Sensor::DropNewest) {
            while (frameQueue.size() >= frameLimit) {
                sensor->decShotsPending();
                delete frameQueue.pullBack();
            }
        } else {
            error(Event::InternalError, sensor,
                  "Unknown drop policy! Not dropping frames.\n");
        }
    }
}

#if 1
void Daemon::runSetter() {
  printf("Dummy runSetter, sleeping\n");
  sleep(100);
  return;
}
#else
void Daemon::runSetter() {
    dprintf(2, "Running setter...\n");
    fflush(stdout);
    tickSetter(Time::now());
    while (!stop) {
        struct timeval t;
        if (ioctl(daemon_fd, VIDIOC_FCAM_WAIT_FOR_HS_VS, &t)) {
            if (stop) { break; }
            error(Event::DriverError, sensor,
                  "error in VIDIOC_FCAM_WAIT_FOR_HS_VS: %s", strerror(errno));
            usleep(100000);
	    tickSetter(Time::now());
	    continue;
        }
        tickSetter(Time(t));
    }
    // When shutting down, make sure we leave V4L2 with the last requested Shot's
    // settings, so that they can be used by later programs. Otherwise, the bubbles
    // produced during shutdown will clobber them. Gain and WB are not written by bubbles
    // so don't need to be dealt with here.
    if (lastGoodShot.wanted == true) {
        v4l2Sensor->setFrameTime(lastGoodShot.frameTime);
        v4l2Sensor->setExposure(lastGoodShot.exposure);
        v4l2Sensor->setGain(lastGoodShot.gain);
    }
}
#endif

void Daemon::modeSwitch(_Frame *req) {

        // flush the pipeline
        dprintf(3, "Setter: Mode switch required - flushing pipe\n");
        pipelineFlush = true;

        pthread_mutex_lock(&cameraMutex);
        dprintf(3, "Setter: Handler done flushing pipe, passing control back to setter\n");

        // do the mode switch

        if (current.image.width() > 0) {
            dprintf(3, "Setter: Shutting down camera\n");
            v4l2Sensor->stopStreaming();
            v4l2Sensor->close();
        }
        dprintf(3, "Setter: Starting up camera in new mode\n");
        v4l2Sensor->open();

        // set all the params for the new frame
        V4L2Sensor::Mode m;
        m.width  = req->shot().image.width();
        m.height = req->shot().image.height();
        // enforce UYVY or RAW
        m.type   = req->shot().image.type() == RAW ? RAW : UYVY;
        v4l2Sensor->startStreaming(m, req->shot().histogram, req->shot().sharpness);
	printf("frametime(0)\n");
        v4l2Sensor->setFrameTime(0);

	printf("fback to handler,\n");
        dprintf(3, "Setter: Setter done bringing up camera, passing control back "
                "to handler. Expect two mystery frames.\n");
        pipelineFlush = false;
        pthread_mutex_unlock(&cameraMutex);

	printf("discarding 2\n");
        m = v4l2Sensor->getMode();
        // Set destination image to new mode settings
        req->image = Image(m.width, m.height, m.type, Image::Discard);

        current._shot.image = Image(req->shot().image.size(), req->shot().image.type(), Image::Discard);
        current._shot.histogram  = req->shot().histogram;
        current._shot.sharpness  = req->shot().sharpness;

        // make sure we set everything else for the next frame,
        // because restarting streaming will have nuked it
        current._shot.frameTime = -1;
        current._shot.exposure = -1;
        current._shot.gain = -1.0;
        current._shot.whiteBalance = -1;
        current.image = Image(m.width, m.height, m.type, Image::Discard);
}

void Daemon::tickSetter(Time hs_vs) {
    static _Frame *req = NULL;

    dprintf(3, "Current hs_vs was at %d %d\n", hs_vs.s(), hs_vs.us());

    // how long will the previous frame take to readout
    static int lastReadout = 33000;

    static bool ignoreNextHSVS = false;

    dprintf(4, "Setter: got HS_VS\n");

    if (ignoreNextHSVS) {
        dprintf(4, "Setter: ignoring it\n");
        ignoreNextHSVS = false;
        return;
    }

    // Is there a request for which I have set resolution and exposure, but not gain and WB?
    if (req) {
        dprintf(4, "Setter: setting gain and WB\n");
        // set the gain and predicted done time on the pending request
        // and then push it onto the handler's input queue and the v4l2 buffer queue
        // Only update gain/wb for wanted requests, bubbles shouldn't change these.
        if (req->shot().wanted == true) {
            if (req->shot().gain != current._shot.gain) {
                v4l2Sensor->setGain(req->shot().gain);
                current._shot.gain = req->shot().gain;
            }
            current.gain = req->gain = v4l2Sensor->getGain();

            if (req->shot().colorMatrix().size() != 12) {
                if (req->shot().whiteBalance != current._shot.whiteBalance) {
                    int wb = req->shot().whiteBalance;

                    // Very lenient sanity checks - restricting the range is up to the auto-wb algorithm
                    if (wb < 0) { wb = 0; } // a black-body radiator at absolute zero.
                    if (wb > 25000) { wb = 25000; } // A type 'O' star.

                    float matrix[12];
                    sensor->platform().rawToRGBColorMatrix(wb, matrix);
                    v4l2Sensor->setWhiteBalance(matrix);

                    current._shot.whiteBalance = req->shot().whiteBalance;
                    current.whiteBalance = wb;
                }
                req->whiteBalance = current.whiteBalance;
            } else {
                // custom WB - always set it
                v4l2Sensor->setWhiteBalance(&req->shot().colorMatrix()[0]);
                current._shot.setColorMatrix(&req->shot().colorMatrix()[0]);
            }
        }

        // Predict when this frame will be done. It should be HS_VS +
        // the readout time for the previous frame + the frame time
        // for this request + however long the ISP takes to process
        // this frame.
        req->processingDoneTime = hs_vs;

        // first there's the readout time for the previous frame
        req->processingDoneTime += lastReadout;

        // then there's the time to expose and read out this frame
        req->processingDoneTime += req->frameTime;

        // then there's some significant time inside the ISP if it's YUV and large
        // (this may be N900 specific)
        int ispTime = 100000;
        if (req->image.type() == UYVY) {
            if (req->image.height() > 1024 && req->image.width() > 1024) {
                ispTime = 65000;
            } else {
                ispTime = 10000;
            }
        }
        req->processingDoneTime += ispTime;

        // Also compute when the exposure starts and finishes
        req->exposureStartTime = hs_vs + req->frameTime - req->exposure;

        // Now updated the readout time for this frame. This formula
        // is specific to the toshiba et8ek8 sensor on the n900
        lastReadout = (current.image.height() > 1008) ? 76000 : 33000;

        req->exposureEndTime  = req->exposureStartTime + req->exposure + lastReadout;

        // now queue up this request's actions
        pthread_mutex_lock(&actionQueueMutex);
        for (std::set<FCam::Action *>::const_iterator i = req->shot().actions().begin();
             i != req->shot().actions().end();
             i++) {
            Action a;
            a.time = req->exposureStartTime + (*i)->time - (*i)->latency;
            a.action = (*i)->copy();
            actionQueue.push(a);
        }
        pthread_mutex_unlock(&actionQueueMutex);
        for (size_t i = 0; i < req->shot().actions().size(); i++) {
            sem_post(&actionQueueSemaphore);
        }

        // The setter is done with this frame. Push it into the
        // in-flight queue for the handler to deal with.
        inFlightQueue.push(req);
        req = NULL;
    } else {
        // a bubble!
        lastReadout = (current.image.height() > 1008) ? 76000 : 33000;
    }

    // grab a new request and set an appropriate exposure and
    // frame time for it make sure there's a request ready for us
    if (!requestQueue.size()) {
        sensor->generateRequest();
    }

    // Peek ahead into the request queue to see what request we're
    // going to be handling next
    if (requestQueue.size()) {
        dprintf(4, "Setter: grabbing next request\n");
        // There's a real request for us to handle
        req = requestQueue.front();
        // Keep track of the last good shot parameters so we can set those on exit
        // since bubbles will clobber the V4L2 state on shutdown otherwise.
        // This allows us to be nice to other programs hoping to use our last settings.
        if (req->_shot.wanted == true) {
            lastGoodShot.wanted = true;
            lastGoodShot.exposure = req->_shot.exposure;
            lastGoodShot.gain = req->_shot.gain;
            lastGoodShot.frameTime = req->_shot.frameTime;
        }
    } else {
        dprintf(4, "Setter: inserting a bubble\n");
        // There are no pending requests, push a bubble into the
        // pipeline. The default parameters for a frame work nicely as
        // a bubble (as short as possible, no stats generated, output
        // unwanted).
        req = new _Frame;
        req->_shot.wanted = false;

        // bubbles should just run at whatever resolution is going. If
        // fast mode switches were possible, it might be nice to run
        // at the minimum resolution to go even faster, but they're
        // not.
        req->_shot.image = Image(current._shot.image.size(), current._shot.image.type(), Image::Discard);

        // generate histograms and sharpness maps if they're going,
        // but drop the data.
        req->_shot.histogram  = current._shot.histogram;
        req->_shot.sharpness  = current._shot.sharpness;

        // push the bubble into the pipe
        requestQueue.pushFront(req);
    }

    // Check if the next request requires a mode switch
    if (req->shot().image.size() != current._shot.image.size() ||
        req->shot().image.type() != current._shot.image.type() ||
        req->shot().histogram  != current._shot.histogram  ||
        req->shot().sharpness  != current._shot.sharpness) {

      modeSwitch(req);

        req = NULL;

        // Wait for the second HS_VS before proceeding
        ignoreNextHSVS = true;

        return;
    } else {
        // no mode switch required
    }

    // pop the request
    requestQueue.pop();

    Time next = hs_vs + current.frameTime;
    dprintf(3, "The current %d x %d frame has a frametime of %d\n",
            current.image.width(), current.image.height(), current.frameTime);
    dprintf(3, "Predicting that the next HS_VS will be at %d %d\n",
            next.s(), next.us());

    int exposure = req->shot().exposure;
    int frameTime = req->shot().frameTime;

    if (frameTime < exposure + 400) {
        frameTime = exposure + 400;
    }

    dprintf(4, "Setter: setting frametime and exposure\n");
    // Set the exposure
    v4l2Sensor->setFrameTime(frameTime);
    v4l2Sensor->setExposure(exposure);

    // Tag the request with the actual params. Also store them in
    // current to avoid unnecessary I2C.
    current._shot.frameTime = frameTime;
    current._shot.exposure  = exposure;
    current.exposure  = req->exposure  = v4l2Sensor->getExposure();
    current.frameTime = req->frameTime = v4l2Sensor->getFrameTime();
    req->image = current.image;

    // Exposure and frame time are set. Return and wait for the next
    // HS_VS before setting gain for this request-> (and pulling the next request).

    dprintf(4, "Setter: Done with this HS_VS, waiting for the next one\n");

}

  void Daemon::setControl(int fd, unsigned int id, int value) {
    v4l2_control ctrl;
    ctrl.id = id;
    ctrl.value = value;
    if (ioctl(fd, VIDIOC_S_CTRL, &ctrl) < 0) {
        // TODO: Better error reporting for all the get/set
      dprintf(0, "VIDIOC_S_CTRL failed: %m\n");
        error(Event::DriverError, "VIDIOC_S_CTRL: %s", strerror(errno));
        return;
    }
}
  
  int Daemon::getBest(void)
  {
      int max = 0;
      int maxi = -1;
      for (int i = 0; i < 1024; i++) {
	if (sharpnessMap[i] != -1) {
	  if (sharpnessMap[i] > max) {
	    max = sharpnessMap[i];
	    maxi = i;
	  }
	}
      }
      return maxi;
  }
  
  void Daemon::setGainExp(int &gain, int &exp)
  {
    if (gain_fd < 0) 
      gain_fd = open("/dev/v4l-subdev12", O_RDWR);

    if (gain_fd < 0) {
      error(Event::InternalError, sensor, "Error opening %s: %d", "/dev/v4l-subdev12", errno);
        return;
    }
    if (gain < 0)
      gain = 0;
    if (gain > 40)
      gain = 40;
    setControl(gain_fd, 0x00980913, gain);

    if (exp < 50)
      exp = 50;
    if (exp > 33267)
      exp = 33267;
    setControl(gain_fd, 0x00980911, exp);
  }

  void Daemon::setFocus(int where)
  {
    if (focus_fd < 0) 
      focus_fd = open("/dev/v4l-subdev8", O_RDWR);

    if (focus_fd < 0) {
      error(Event::InternalError, sensor, "Error opening %s: %d", "/dev/v4l-subdev8", errno);
        return;
    }
    setControl(focus_fd, 0x009a090a, where % 1024);
  }

  void saveJPEG(FCam::Image &img, char *phase)
  {
    static int start;
    static char buf[1024];
    if (!start)
      start = time(NULL);

    std::string name;
#if 0
    snprintf(buf, sizeof(buf)-1, "/data/tmp/img-%d-%s.dng", start, phase);
    name=buf;
    FCam::saveDNG(img, name);
#endif
    snprintf(buf, sizeof(buf)-1, "/data/tmp/img-%d-%s.jpg", start, phase);
    name=buf;
    if (!d_raw)
      FCam::saveJPEG(img, name);
  }

  void Daemon::imageStatistics(FCam::Image &img)
{
  int sx, sy;
  static Conv c;

  sx = img.size().width;                                                          
  sy = img.size().height;                                                         
  //printf("Image is %d x %d, ", sx, sy);                                           

  unsigned char *middle = img(sx/2, sy/2);                                        

  printf("(%d) ", brightness(middle));                 

  int dark = 0;                                                                   
  int bright = 0;                                                                 
  int total = 0;                                                                  
#define RATIO 10
  for (int y = sy/(2*RATIO); y < sy; y += sy/RATIO)                               
    for (int x = sx/(2*RATIO); x < sx; x += sx/RATIO) {                    
      int br = brightness(img(x, y));                               

      if (!d_raw) {
	/* It seems real range is 60 to 218 */
	if (br > 200)
	  bright++;                                               
	if (br < 80)
	  dark++;
      } else {
	/* there's a lot of noise, it seems black is commonly 65..71,
	   bright is cca 1023 */
	if (br > 1000)
	  bright++;
	if (br < 75)
	  dark++;
      }
      total++;                                                      
    }

  printf("%d dark %d bri,", dark, bright);                     

  long sharp = 0;
  for (int y = sy/3; y < 2*(sy/3); y+=sy/12) {
    int b = -1;                                                                     
    for (int x = sx/3; x < 2*(sx/3); x++) {                                         
      int b2;                                                                
      b2 = brightness(img(x, y/2));                                          
      if (b != -1)
	sharp += (b-b2) * (b-b2);                                              
      b = b2;                                                                
    }
  }
  printf("sh %d @ ", sharp);
  printf("%.3fm (%d)", c.focus_to_m(currentFocus), currentFocus);
  printf("- g/e %d/%d, ", currentGain, currentExposure);
  c.print(currentGain, currentExposure);
  printf("\n");

  if (phase > 4)
    sharpnessMap[currentFocus] = sharp;

  int bad = 0;

  {
    static SDLOut *out = NULL;

    if (!out) {
      out = new SDLOut(img);
    }

    out->focus = currentFocus / 1023.;
    out->gain = currentGain / 40.;
    out->time = currentExposure /34000.;
    out->render(img);
  }

  /* It takes cca 10 substeps for settings to take effect?! */

  switch (phase) {
  case 0:
    phase = 1;
  case 1:
    setFocus(currentFocus);
    if (!bright && dark) {
      if (currentExposure > 5000)
	currentGain ++;
      /* Should use currentExposure * (2 ** .1)
	 or * 1.0717734625362931 */
      currentExposure += 1 + (currentExposure * 717) / 10000;
      currentExposure += 1 + (currentExposure * 717) / 10000;
      currentExposure += 1 + (currentExposure * 717) / 10000;
      bad = 1;
    }
    if (bright && !dark) {
      currentGain --;
      currentExposure -= 1 + (currentExposure * 654) / 10000;
      currentExposure -= 1 + (currentExposure * 654) / 10000;
      currentExposure -= 1 + (currentExposure * 654) / 10000;
      bad = 1;
    }
    setGainExp(currentGain, currentExposure);
    subphase ++;
    if (subphase == 10) {
      saveJPEG(img, "pre");
    }
    if ((currentGain == 40) && (currentExposure == 33267)) {
      printf("It is too dark to take photos here!!!\n");
      bad = 0;
    }
    if ((subphase > 20) && (!bad)) {
      saveJPEG(img, "gain");
      phase = 5;
    }
    break;
  case 5: 
    focusMin = 0;
    focusMax = 1023;
    currentFocus = (currentFocus + 30);
    if (currentFocus > 1024) {
      subphase = 0;
      phase = 6;
      int maxi = getBest();
      printf("Preliminary best @ %d, %.2fm\n", maxi, c.diopter_to_m(c.focus_to_diopter(maxi)));
      focusMin = maxi - 20;
      focusMax = maxi + 20;
      currentFocus = focusMin;
      setFocus(currentFocus);
    }
    setFocus(currentFocus);
    break;
  case 6:
    subphase++;
    if (subphase == 20) {
      phase = 7;
      saveJPEG(img, "af1");
    }
    break;
  case 7: 
    currentFocus = (currentFocus + 1);
    if (currentFocus > focusMax) {
      int maxi = getBest();
      printf("Final best @ %d, %.2fm\n", maxi, c.diopter_to_m(c.focus_to_diopter(maxi)));
      currentFocus = maxi;
      subphase = 0;
      phase = 8;
    }
    setFocus(currentFocus);
    break;
  case 8:
    subphase++;
    if (subphase == 20) {
      phase = 10;
      saveJPEG(img, "af2");
    }
    break;
  case 10:
#if 0
    for (int i = 0; i < 1024; i++) {
      if (sharpnessMap[i] != -1) {
	printf("%d %d\n", i, sharpnessMap[i]);
      }
    }
#endif
    exit(0);
  }
}

void Daemon::runHandler() {
    _Frame *req = NULL;
    V4L2Sensor::V4L2Frame *f = NULL;

    printf("runHandler: getting mutex\n");

    req = requestQueue.pull();
    printf("mode switching\n");
    modeSwitch(req);

    pthread_mutex_lock(&cameraMutex);

    printf("Discarding mystery 1\n");
    f = v4l2Sensor->acquireFrame(true);
    v4l2Sensor->releaseFrame(f);
    printf("Discarding mystery 2\n");
    f = v4l2Sensor->acquireFrame(true);
    v4l2Sensor->releaseFrame(f);
    printf("Discarding mystery 3\n");
    f = v4l2Sensor->acquireFrame(true);
    v4l2Sensor->releaseFrame(f);

    while (!stop) {
      //printf("runHandler: looping\n");
        // the setter may be waiting for me to finish processing
        // outstanding requests
        if (!req && pipelineFlush && inFlightQueue.size() == 0) {
            dprintf(3, "Handler: Handler done flushing pipe, passing control back to setter\n");
            while (pipelineFlush) {
                pthread_mutex_unlock(&cameraMutex);
                // let the setter grab the mutex. It has higher priority,
                // so it should happen instantly. We put this in a while
                // loop just to be sure.
                usleep(10000);
                pthread_mutex_lock(&cameraMutex);
            }
            dprintf(3, "Handler: Setter done bringing up camera, passing control back to handler\n");

        }

        if (pipelineFlush) {
            dprintf(3, "Handler: Setter would like me to flush the pipeline, but I have requests in flight\n");
        }

        // wait for a frame
        if (!f) {
            f = v4l2Sensor->acquireFrame(true);
        }

        if (!f) {
	  dprintf(0, "Handler got a NULL frame\n");
            error(Event::InternalError, "Handler got a NULL frame\n");
            usleep(300000);
            continue;
        }

#if 0
        // grab a request to match to it
        if (!req) {
            if (inFlightQueue.size()) {
                dprintf(4, "Handler: popping a frame request\n");
                req = inFlightQueue.pull();
            } else {
                // there's no request for this frame - probably coming up
                // from a mode switch or starting up
                dprintf(3, "Handler: Got a frame without an outstanding request,"
                        " dropping it.\n");
                v4l2Sensor->releaseFrame(f);
                f = NULL;
                continue;
            }
        }
#endif
#if 0
	if (!req) {
	  static N900::_Frame frame;
	  printf("....Looking for request: %d remaining\n", requestQueue.size());
	  //req = requestQueue.pull();
	  req = &frame;
	  if (!req)
	    return;
	}
#endif
        // at this point we have a frame and a request, now look at
        // the time delta between them to see if they're a match
        int dt = req->processingDoneTime - f->processingDoneTime;

        //dprintf(4, "Handler dt = %d\n", dt);

        if (0) {
            dprintf(3, "Handler: Expected a frame at %d %d, but one didn't arrive until %d %d\n",
                    req->processingDoneTime.s(), req->processingDoneTime.us(),
                    f->processingDoneTime.s(), f->processingDoneTime.us());
            error(Event::ImageDroppedError, sensor,
                  "Expected image data not returned from V4L2. Likely caused by excess"
                  " page faults (thrashing).");
            req->image = Image(req->image.size(), req->image.type(), Image::Discard);
            if (!req->shot().wanted) {
                delete req;
            } else {
                // the histogram and sharpness map may still have appeared
                req->histogram = v4l2Sensor->getHistogram(req->exposureEndTime, req->shot().histogram);
                req->sharpness = v4l2Sensor->getSharpnessMap(req->exposureEndTime, req->shot().sharpness);
                frameQueue.push(req);
                enforceDropPolicy();
            }
            req = NULL;
        } else if (1) {
            // Is this frame wanted or a bubble?
            if (!req->shot().wanted) {
                // it's a bubble - drop it
                dprintf(4, "Handler: discarding a bubble\n");
                delete req;
                v4l2Sensor->releaseFrame(f);
            } else {

	      //printf("Found a match -- bag & tag\n");
                // this looks like a match - bag and tag it
                req->processingDoneTime = f->processingDoneTime;

                size_t bytes = req->image.width()*req->image.height()*2;
                if (f->length < bytes) { bytes = f->length; }

                if (req->shot().image.autoAllocate()) {
                    req->image = Image(req->image.size(), req->image.type(), f->data).copy();
                } else if (req->shot().image.discard()) {
                    req->image = Image(req->image.size(), req->image.type(), Image::Discard);
                } else {
                    if (req->image.size() != req->shot().image.size()) {
                        error(Event::ResolutionMismatch, sensor,
                              "Requested image size (%d x %d) "
                              "on an already allocated image does not "
                              "match actual image size (%d x %d). Dropping image data.",
                              req->shot().image.width(), req->shot().image.height(),
                              req->image.width(), req->image.height());
                        req->image = Image(req->image.size(), req->image.type(), Image::Discard);
                        // TODO: crop instead?
                    } else if (req->image.type() != req->shot().image.type()) {
                        error(Event::FormatMismatch, sensor,
                              "Requested unsupported image format %d "
                              "for an already allocated image.",
                              req->shot().image.type());
                        req->image = Image(req->image.size(), req->image.type(), Image::Discard);
                    } else { // the size matches
		      static int num;
		      std::string fname;
		      std::ostringstream convert;
                        req->image = req->shot().image;
                        // figure out how long I can afford to wait
                        // For now, 10000 us should be safe
                        Time lockStart = Time::now();
			//printf("Copying image data (%d)\n", (int) bytes);
			  FCam::Image img = Image(req->image.size(), req->image.type(), f->data);
			  req->image = img;
			  //                            req->image.copyFrom();
			  //req->image.unlock();
			  convert << "/data/tmp/frame-" << num++ << ".dng";
			  fname = convert.str();
			  printf("# %d: ", num);
			  imageStatistics(img);

			  if (d_raw)
			    if (!((num - 0) % 120)) {
			    //			    FCam::saveJPEG(img, fname);
			      FCam::saveDNG(req, fname);

			      printf("dng saved\n");
			      convert << ".jpg";
			      fname = convert.str();
			      FCam::saveJPEG(img, fname);
			      printf("jpeg saved\n");
			    }

			  //printf("Faked image save, continuing\n");
			    v4l2Sensor->releaseFrame(f);

			    f = NULL;
			    continue;

                        if (req->image.lock(10000)) {

                        } else {
                            warning(Event::ImageTargetLocked, sensor,
                                    "Daemon discarding image data (target is still locked, "
                                    "waited for %d us)\n", Time::now() - lockStart);
                            req->image = Image(req->image.size(), req->image.type(), Image::Discard);
                        }
                    }
                }

                v4l2Sensor->releaseFrame(f);
                req->histogram = v4l2Sensor->getHistogram(req->exposureEndTime, req->shot().histogram);
                req->sharpness = v4l2Sensor->getSharpnessMap(req->exposureEndTime, req->shot().sharpness);

                frameQueue.push(req);
                enforceDropPolicy();

            }

            req = NULL;
            f = NULL;

        } else { // more than 10ms early. Perhaps there was a mode switch.
            dprintf(3, "Handler: Received an early mystery frame (%d %d) vs (%d %d), dropping it.\n",
                    f->processingDoneTime.s(), f->processingDoneTime.us(),
                    req->processingDoneTime.s(), req->processingDoneTime.us());
            v4l2Sensor->releaseFrame(f);
            f = NULL;
        }

    }
    pthread_mutex_unlock(&cameraMutex);

}


void Daemon::runAction() {
    dprintf(2, "Action thread running...\n");
    while (1) {
        sem_wait(&actionQueueSemaphore);
        if (stop) { break; }
        // priority inversion :(
        pthread_mutex_lock(&actionQueueMutex);
        Action a = actionQueue.top();
        actionQueue.pop();
        pthread_mutex_unlock(&actionQueueMutex);

        Time t = Time::now();
        int delay = (a.time - t) - 500;
        if (delay > 0) { usleep(delay); }
        Time before = Time::now();
        // busy wait until go time
        while (a.time > before) { before = Time::now(); }
        a.action->doAction();
        //Time after = Time::now();
        dprintf(3, "Action thread: Initiated action %d us after scheduled time\n", before - a.time);
        delete a.action;
    }
}

}
}

