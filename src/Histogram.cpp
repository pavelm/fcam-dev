#include "FCam/Histogram.h"
#include <stdio.h>
#include <cmath>

namespace FCam {

void Histogram::debug(const char *name) const {
    printf("\tHistogram %s at %px with buckets %d, channels %d\n"
           "\t  valid: %s\n"
           "\t  region: start at (%d, %d), size (%d,%d)\n",
           name, this, buckets(), channels(), valid() ? "yes" : "NO",
           region().x, region().y, region().width, region().height);
    if (valid()) {
        unsigned maxCount = 1;
        for (unsigned int i=0; i < buckets(); i++) {
            unsigned count = (*this)(i);
            if (count > maxCount) { maxCount = count; }
        }
        // Print 20-wide vertical histogram
        printf("\t    0%18c%d\n",' ',maxCount);
        printf("\t    |%18c|\n",' ');
        float tickSize = maxCount / 20;
        for (unsigned int i=0; i < buckets(); i++) {
            unsigned count = (*this)(i);
            count = static_cast<unsigned>(std::floor((count / tickSize) + 0.5f));
            printf("\t%03d ", i);
            for (unsigned int c = 0; c < count; c++) {
                printf("#");
            }
            printf("\n");
        }
    }
}

}
