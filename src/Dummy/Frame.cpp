#include <FCam/Dummy/Frame.h>
#include <FCam/processing/Color.h>

namespace FCam {
namespace Dummy {

// _Frame vtable lives here
_Frame::_Frame() {
    // Set basic Platform fields to reasonable defaults
    _manufacturer = "Stanford University";
    _model = "Dummy test platform";
    _bayerPattern = GRBG;
    _minRawValue = 0;
    _maxRawValue = 1024;
    // Set up color matrices as identity
    for (int i=0; i<12; i++) {
        rawToRGB3200K[i] = 0.f;
        rawToRGB7000K[i] = 0.f;
    }
    rawToRGB3200K[0] = 1.f;
    rawToRGB3200K[5] = 1.f;
    rawToRGB3200K[10] = 1.f;
    rawToRGB7000K[0] = 1.f;
    rawToRGB7000K[5] = 1.f;
    rawToRGB7000K[10] = 1.f;

}

void _Frame::rawToRGBColorMatrix(int kelvin, float *matrix) const {
    // Linear interpolation with inverse color temperature
    float alpha = (1./kelvin-1./3200)/(1./7000-1./3200);
    colorMatrixInterpolate(rawToRGB3200K,
                           rawToRGB7000K,
                           alpha, matrix);
}

}
}
