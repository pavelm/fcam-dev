#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <vector>

// Select the platform
#include <FCam/N900.h>
namespace Plat = FCam::N900;

/** \file */

/***********************************************************/
/* A program that takes a single shot                      */
/*                                                         */
/* This example is a simple demonstration of the usage of  */
/* the FCam API.                                           */
/***********************************************************/

void errorCheck();

extern int d_argc, d_raw;
extern char **d_argv;

int main(int argc, char **argv) {
  int raw = 0, sx = 1296, sy = 984;
  d_argc = argc;
  d_argv = argv;
  
  for (int i=1; i<argc; i++) {
    if (!strncmp(argv[i], "preview:", 4)) {
      if (2 != sscanf(argv[i], "preview:%dx%d", &sx, &sy))
	printf("Bad parameters?\n");
      raw = 0;
    }
    if (!strncmp(argv[i], "raw:", 4)) {
      if (2 != sscanf(argv[i], "raw:%dx%d", &sx, &sy))
	printf("Bad parameters?\n");
      raw = 1;
    }
  }
  d_raw = raw;

  printf("Init: sensor\n");

    // Make the image sensor
    Plat::Sensor sensor;

  printf("Init: shot\n");
    // Make a new shot
    std::vector<FCam::Shot> shot(1);
    for (size_t i = 0; i < shot.size(); i++) {
      printf("Allocating image %d\n", i);

      /* Does not yet work:
	 [ 5698.183746] omap3isp 480bc000.isp: CCDC won't become idle!
	 [ 5775.415740] omap3isp 480bc000.isp: Unable to stop OMAP3 ISP CCDC
      */
      //shot[i].image = FCam::Image(2592, 1968, FCam::RAW);
      //shot[i].image = FCam::Image(1296, 984, FCam::RAW);
      // There's something funny going on with 640x492: right part of picture is 
      // blank, strange interference pattern
      //shot[i].image = FCam::Image(640, 492, FCam::RAW);

      if (raw) {
	printf("RAW %dx%d\n", sx, sy);
	shot[i].image = FCam::Image(sx, sy, FCam::RAW);
      } else {
	printf("UYVY %dx%d\n", sx, sy);
	shot[i].image = FCam::Image(sx, sy, FCam::UYVY);
      }

      //shot[i].image = FCam::Image(2592, 1968, FCam::UYVY); -- does not work, 4x image
      //shot[i].image = FCam::Image(1200, 900, FCam::UYVY);
      //shot[i].image = FCam::Image(800, 600, FCam::UYVY);
      // shot[i].image = FCam::Image(640, 480, FCam::UYVY);
      //shot[i].image = FCam::Image(160, 120, FCam::UYVY);
        shot[i].exposure = 50000; // 50 ms exposure
        shot[i].gain = 1.0f;      // minimum ISO
    }

  printf("Init: go\n");

  printf("Capture\n");
    // Order the sensor to capture the shots
    sensor.capture(shot);
#if 1
  printf("Errors\n");
    // Check for errors
    errorCheck();

    assert(sensor.shotsPending() == shot.size()); // There should be exactly this many shots
  printf("Get frames\n");
    // Retrieve the frame from the sensor
    std::vector<FCam::Frame> frame(shot.size());
    for (size_t i = 0; i < shot.size(); i++) {
        frame[i] = sensor.getFrame();
    }


    sensor.stop();

    errorCheck();

    // Each frame should be the result of the shot we made
    for (size_t i = 0; i < shot.size(); i++) {
      printf("Checking frame %d: %d == %d\n", i, frame[i].shot().id, shot[i].id);
	//	printf("%lx \n", frame[i].shot().id);
	printf("Shot?\n");
	//assert(frame[i].shot().id == shot[i].id);

	printf("Frame...\n");
        // This frame should be valid too
        assert(frame[i].valid());
	printf("Image... valid=%d\n", frame[i].image().valid());
	printf("Image... data=%lx\n", frame[i].image().data);
	/* 0 == Image::Discard */

        printf("Frame %d OK!\n", i);
    }

    //#define DCIM "/home/user/MyDocs/DCIM/"
#define DCIM ""

    FCam::saveJPEG(frame[0], DCIM "frame0.jpg");

    // Save the frames
    FCam::saveDNG(frame[0], DCIM "frame0.dng");
    //    FCam::saveDNG(frame[1], DCIM "frame1.dng");
    //    FCam::saveDNG(frame[2], DCIM "frame2.dng");

    // Check that the pipeline is empty
    assert(sensor.framesPending() == 0);
    assert(sensor.shotsPending() == 0);
#endif
    return 0;
}




void errorCheck() {
    // Make sure FCam is running properly by looking for DriverError
    FCam::Event e;
    while (FCam::getNextEvent(&e, FCam::Event::Error)) {
        printf("Error: %s\n", e.description.c_str());
        if (e.data == FCam::Event::DriverMissingError) {
            printf("example1: FCam can't find its driver. Did you install "
                   "fcam-drivers on your platform, and reboot the device "
                   "after installation?\n");
            exit(1);
        }
        if (e.data == FCam::Event::DriverLockedError) {
            printf("example1: Another FCam program appears to be running "
                   "already. Only one can run at a time.\n");
            exit(1);
        }
    }
}
